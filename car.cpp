#include "car.h"

#include <iostream>
#include <sstream>

Engine::Engine(const string& id, const string& companyName,
 const string& model, double volume, int yearOfProduction)
: id(id), companyName(companyName), model(model), volume(volume), yearOfProduction(yearOfProduction)
{}

void Engine::stop()
{
    std::cout << "The engine has stopped." << std::endl;
}

string Engine::getInformation()
{
    std::stringstream ss;
    ss<<volume;
    string volstring;
    string yearstring;
    ss>>volstring;
    ss.clear();
    ss<<yearOfProduction;
    ss>>yearstring;
    return '['+id+'|'+companyName+'|'+model+'|'+volstring+'|'+yearstring+']';
}

BMWEngine::BMWEngine(const string& id, const string& model, double volume, int yearOfProduction)
 : Engine(id, "BMW", model, volume, yearOfProduction) {}


void BMWEngine::start() 
{
    std::cout << "The BMW engine has started" << std::endl;
}

void BMWEngine::stop()
{
    std::cout << "The BMW has stopped.";
}

VWEngine::VWEngine(const string& id, const string& model, double volume, int yearOfProduction)
: Engine(id, "Volkswagen", model, volume, yearOfProduction) {}

void VWEngine::start() 
{
    std::cout << "VW engine has started" << std::endl;
}

Car::Car(Engine* engine) : engine(engine) {}

void Car::printEngineInformation()
{
    std::cout << engine->getInformation() << std::endl;
}
