#include "car.h"

#include <iostream>

int main()
{
    std::cout << "Choose engine [bmw/vw]: ";
    string engine;
    std::cin>>engine;
    while (engine!="bmw"&&engine!="vw")
    {
        std::cout << "Insert correct name [bmw/vw]: ";
        std::cin>>engine;
    }
    
    Car car;
    if (engine=="bmw")
    {
        car.setEngine(new BMWEngine("12345", "f120", 6.0, 2020));
    }
    else
    {
        car.setEngine(new VWEngine("54321", "GOLF", 1.2, 1998));
    }
    car.start();
    car.printEngineInformation();

    return 0;
}