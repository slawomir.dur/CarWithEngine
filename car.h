#ifndef CAR_H
#define CAR_H

#include <string>
using std::string;

class Engine
{
    protected:
        string id;
        string companyName;
        string model;
        double volume;
        int yearOfProduction;  
    public:
        Engine()=default;
        Engine(const string& id, const string& companyName, const string& model, double volume, int yearOfProduction);
        Engine(const Engine& other)=default;
        virtual ~Engine()=default;
        virtual void start()=0;
        virtual void stop();
        string getCompanyName() {return companyName;}
        string getModel() {return model;}
        double getVolume() {return volume;}
        int getProductionYear() {return yearOfProduction;}
        string getId() {return id;}
        string getInformation();
};

class BMWEngine : public Engine
{
    public:
        BMWEngine(const string& id, const string& model, double volume, int yearOfProduction);
        BMWEngine(const BMWEngine& other)=default;
        virtual void start() override;
        virtual void stop() override;
};

class VWEngine : public Engine
{
    public:
        VWEngine(const string& id, const string& model, double volume, int yearOfProduction);
        VWEngine(const VWEngine& other)=default;
        virtual void start() override;
};




class Car
{
    private:
        Engine* engine;
    public:
        Car(Engine* engine);
        Car()=default;
        void setEngine(Engine* engine) {this->engine=engine;}
        void start() {engine->start();}
        void printEngineInformation();
};



#endif